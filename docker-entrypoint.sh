#!/bin/bash

start_server()
{

  echo "Unzipping mod"
  unzip $MINECRAFT_MOD_ZIP -d /tmp
  echo "Copying files"
  cp -rf /tmp/* $MINECRAFT_HOME
  rm -rf /tmp

  echo "Copying libraries"  
  cd $MINECRAFT_HOME
  cp ../libraries.tar.gz .
  tar xvf libraries.tar.gz
  rm libraries.tar.gz

  echo "Running install jar"
  chmod 755 settings.sh $INSTALL_JAR
  . ./settings.sh
  java -jar ${INSTALL_JAR} --installServer

  cd ${MINECRAFT_HOME}
  echo "[EULA] Accepting EULA"
  echo "#By changing the setting below to TRUE you are indicating your agreement to our EULA \(https://account.mojang.com/documents/minecraft_eula\)." > eula.txt
  echo "eula=true" >> eula.txt

  MAX_RAM=9g

  java -server -Xms${MIN_RAM} -Xmx${MAX_RAM} ${JAVA_PARAMETERS} -jar ${SERVER_JAR} nogui
}

start_server
